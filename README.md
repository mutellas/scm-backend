# contract-manager-server

## Installation

Make sure you have installed NodeJS.<br>
Make sure you have installed `typescript`.

The clone this repository and do
- `npm install` or `yarn install`
- `npm run build`
- Take a look into `config/server.json`
- `npm start`
- The server should be running

## Start

## Routes

Currently there is only one request. More will be added later.

```
/contracts/get/all - Returns a list of contracts as json string. Take a look into `data/sample-contracts.json` for an overview.
```