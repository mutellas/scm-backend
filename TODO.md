# ToDo List

## Database

- [ ] Filesystem using json files
- [ ] Sqlite

## Routes

### Contracts

- [ ] /contracts/create/new...
- [ ] /contracts/delete/:id
- [ ] /contracts/edit/...
- [x] /contracts/get/all