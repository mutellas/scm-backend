import { ProfileSession } from "../interfaces/ProfileSession";

export interface IProfileActionResult {
    success: boolean
    errors: Array<string|Error>
}

export interface IProfileConnector {
    getByName(name: string): Promise<ProfileSession>
    store(name: string, profile: ProfileSession): Promise<IProfileActionResult>
    remove(name: string): Promise<IProfileActionResult>
}
