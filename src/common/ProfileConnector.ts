import * as path from 'path'
import * as afs from '@rammbulanz/afs'
import { ProfileSession } from '../interfaces/ProfileSession';
import { IProfileConnector, IProfileActionResult } from './IProfileConnector';

const config = require(path.resolve("config/profiles.json")) as {
    fs: {
        root: string
    }
};

export class ProfileConnector implements IProfileConnector {

    protected _loadedProfiles = new Map<string, ProfileSession>();

    public async getByName(name: string) {

        let loadedProfiles = this._loadedProfiles;

        if (loadedProfiles.has(name))
            return loadedProfiles.get(name);

        let profilePath = path.resolve(`${config.fs.root}`)
        let profileFile = path.resolve(`${profilePath}/${name}.json`);

        let profileExists = await afs.exists(profilePath) && await afs.exists(profileFile);

        if (profileExists) {

            let profileJsonEncoded = await afs.readFile(profileFile, "utf8") as string;
            let profile = JSON.parse(profileJsonEncoded) as ProfileSession;

            loadedProfiles.set(name, profile);
            return profile;

        }

        return null;

    }

    public async store(name: string, profile: ProfileSession): Promise<IProfileActionResult> {

        let result: IProfileActionResult = {
            success: false,
            errors: []
        };

        try {

            let profilePath = path.resolve(`${config.fs.root}`)
            let profileFile = path.resolve(`${config.fs.root}/${name}.json`)

            // Check existance of folder and file for this project

            let profilePathExists = await afs.exists(profilePath);
            if (!profilePathExists) {
                await afs.mkdirs(profilePath);
            }

            let profileFileExists = await afs.exists(profileFile);
            if (profileFileExists) {
                await afs.unlink(profileFile);
            }

            // Write data to file

            let data = JSON.stringify(profile, undefined, 2);
            await afs.writeFile(profileFile, data, "utf8");

            // Also apply to cached profile

            this._loadedProfiles.set(name, profile);

            result.success = true;

        } catch (err) {
            console.error(err);
            result.success = false;
            result.errors.push(err);
        }

        return result;
    }

    public async remove(name: string): Promise<IProfileActionResult> {

        let result: IProfileActionResult = {
            success: false,
            errors: []
        };

        try {

            let profilePath = path.resolve(`${config.fs.root}`)
            let profileFile = path.resolve(`${config.fs.root}/${name}.json`)

            // Check existance of folder and file for this project

            let profilePathExists = await afs.exists(profilePath);
            if (!profilePathExists) {
                result.errors.push("Can't remove profile. Profiles folder doesn't exist.");
                result.success = true;
                return result;
            }

            // Delete file if exists

            let profileFileExists = await afs.exists(profileFile);
            if (profileFileExists) {
                await afs.unlink(profileFile);
            }

            // Also apply to cached profile

            this._loadedProfiles.delete(name);

            result.success = true;

        } catch (err) {
            console.error(err);
            result.success = false;
            result.errors.push(err);
        }

        return result;

    }

}