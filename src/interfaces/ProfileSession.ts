export interface ProfileSession {
   name: string
   active: string
   lastLogin: number,
   contracts?: { 
       [id: string]: any
   }
}