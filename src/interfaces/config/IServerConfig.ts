export interface IServerConfig {
    debug: boolean
    database: {
        type: "mongo" | "sqlite" | "fs"
    },
    http?: {
        port: number
    }
    https?: {
        port: number
        // If these are defined they will be used. Otherwise certs object from parent will be used. If they also don't exist, https won't be used.
        pfx: string
        passphrase: string
        key: string
        cert: string
    }
}