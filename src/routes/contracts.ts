import * as KoaRouter from "koa-router";
import { IServerConfig } from "../interfaces/config/IServerConfig";
import { ProfileConnector } from "../common/ProfileConnector";
import { IProfileConnector } from "../common/IProfileConnector";
import * as uuid from 'uuid'


let config = require( "../../config/server.json" ) as IServerConfig;

export const contracts = new KoaRouter();

let profiles: IProfileConnector = new ProfileConnector();
switch ( config.database.type ) {
    case "fs": profiles = new ProfileConnector(); break;
    case "mongo": profiles = null; break;
    case "sqlite": profiles = null; break;
    default:
        console.error( "No valid database type: " + config.database.type );
}


contracts.post( "/contracts/get/all/", async ( ctx ) => {

    let profile = await profiles.getByName( ctx.body.profile );

    if ( !profile || !profile.contracts ) {
        ctx.throw( 404 );
    } else {
        ctx.body = profile.contracts;
    }

} );

contracts.post( "/contracts/create/new", async ( ctx ) => {

    let result: { success: boolean, message?: string, contract?: any } = {
        success: false
    };

    try {

        if ( !ctx.body.profile )
            throw new Error( "No profile name in request" );
        if ( !ctx.body.contract )
            throw new Error( "No contract in request" );

        // Get profile by name from connector

        let profile = await profiles.getByName( ctx.body.profile );

        // Polyfill contract

        let contract = ctx.body.contract;
        contract.id = uuid();

        // Add new contract to profile

        if ( !profile.contracts )
            profile.contracts = {};
        profile.contracts[ contract.id ] = contract;

        // Write changed profile back using the connector
        let writeResult = await profiles.store( ctx.body.profile, profile );
        if ( !writeResult.success ) {
            throw writeResult.errors.join( '\n' );
        }

        result.contract = contract;
        result.success = true;

    } catch ( err ) {
        console.error( err.message || err );
        result.success = false;
        result.message = err.message || err;
    }

    ctx.body = result;

} );

contracts.post( "/contracts/edit", async ctx => {

    let result: { success: boolean, message?: string, contract?: any } = {
        success: false
    };

    try {

        if ( !ctx.body.profile )
            throw new Error( "No profile name in request" );
        if ( !ctx.body.contract )
            throw new Error( "No contract in request" );

        // Get profile by name from connector

        let profile = await profiles.getByName( ctx.body.profile );

        // Polyfill contract

        let contract = ctx.body.contract;

        // Overwrite contract in profile

        profile.contracts[ contract.id ] = contract;

        // Write changed profile back using the connector
        let writeResult = await profiles.store( ctx.body.profile, profile );
        if ( !writeResult.success ) {
            throw writeResult.errors.join( '\n' );
        }

        result.contract = contract;
        result.success = true;

    } catch ( err ) {
        console.error(err.message || err);
        result.success = false;
        result.message = err.message || err;
    }

    ctx.body = result;

} )