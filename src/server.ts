import * as http from 'http'
import * as https from 'https'
import * as Koa from 'koa'
import * as bodyParser from 'koa-bodyparser'
import { IServerConfig } from './interfaces/config/IServerConfig';
import { contracts } from './routes/contracts';
import { readFileSync } from 'fs'
import * as userauth from 'koa-userauth'
import * as session from 'koa-generic-session'

let config = require( "../config/server.json" ) as IServerConfig;
let app = new Koa();
app.keys = [ "I am a secret" ];

app.use( bodyParser() );
app.use( async ( ctx: Koa.Context & { request: { body: object } }, next ) => {
    // the parsed body will store in ctx.request.body
    // if nothing was parsed, body will be an empty object {}
    ctx.body = ctx.request.body;

    await next();
} );

// Session

app.use( session() );
app.use( userauth( {
    match: "/user",
    // auth system login url
    loginURLFormatter: function ( url ) {
        return 'http://login.demo.com/login?redirect=' + url;
    },
    // login callback and getUser info handler
    getUser: async ctx => {
        var token = this.query.token;
        // get user
        return user;
    }
} ) )


// x-response-time

app.use( async ( ctx, next ) => {
    const start = Date.now();
    await next();
    const ms = Date.now() - start;
    ctx.set( 'X-Response-Time', `${ms}ms` );
} );

// logger

app.use( async ( ctx, next ) => {
    const start = Date.now();
    await next();
    const ms = Date.now() - start;
    console.log( `${ctx.method} ${ctx.url} - ${ms}` );
} );

// response

// Enable /contracts router
app.use( contracts.routes() )
    .use( contracts.allowedMethods() );

if ( config.http ) {
    http.createServer( app.callback() ).listen( config.http.port );
    console.log( "HTTP listening on port " + config.http.port );
}

if ( config.https ) {

    try {

        const options: https.ServerOptions = {};

        const is_valid_str = ( str: string ) => {
            return typeof str === "string" && str.length > 0;
        }

        if ( is_valid_str( config.https.key ) && is_valid_str( config.https.cert ) ) {
            options.key = readFileSync( config.https.key );
            options.cert = readFileSync( config.https.cert );
        } else if ( is_valid_str( config.https.pfx ) && is_valid_str( config.https.passphrase ) ) {
            options.pfx = readFileSync( config.https.pfx );
            options.passphrase = config.https.passphrase;
        } else {
            throw "Need ( https.key and https.cert ) or ( https.pfx and https.passphrase ) to be set in config/server.json"
        }

        https.createServer( options, app.callback() ).listen( config.https.port );
        console.log( "HTTPS listening on port " + config.https.port );

    } catch ( err ) {
        console.error( "Failed to start https server: " + ( err.message || err ) );
    }

}

